<?php

namespace GKZF2\Authentication\Role;

class RoleManager {
    /**
     * @param RoleAbstract[] $roles
     * @return RoleAbstract
     */
    public static function getHigherRole($roles) {
        $higherRole = null;
        foreach ($roles as $role) {
            if ($higherRole === null || $role->isHigher($higherRole))  {
                $higherRole = $role;
            }
        }
        return $higherRole;
    }
}