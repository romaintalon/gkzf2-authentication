<?php

namespace GKZF2\Authentication\Role;

use GKZF2\Authentication\Right\Right;
use Zend\Permissions\Acl\Role\GenericRole;

abstract class RoleAbstract extends GenericRole {

    /**
     * @return Right[]
     */
    abstract public function getRights();

    /**
     * @param RoleAbstract $role
     * @return bool
     */
    abstract public function isHigher(RoleAbstract $role);
}