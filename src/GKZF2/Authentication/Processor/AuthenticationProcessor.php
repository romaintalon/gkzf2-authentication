<?php
namespace GKZF2\Authentication\Processor;

use GKZF2\Authentication\Acl;
use GKZF2\Authentication\Model\CurrentLoggedUserGetterInterface;
use GKZF2\Core\Processor\AbstractProcessor;
use GKZF2\Webservice\Processor\FormatterProcessor;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\MvcEvent;

class AuthenticationProcessor extends AbstractProcessor {
    
    public function processAndGetResponse(MvcEvent $e) {
        $this->setProperties($e);

        /** @var AuthenticationService $auth */
        $auth = $this->getServiceManager()->get('GKZF2\Authentication');

        /** @var CurrentLoggedUserGetterInterface $currentLoggedUserGetter */
        $currentLoggedUserGetter = $this->getServiceManager()->get('GKZF2\Authentication\CurrentLoggedUserGetter');
        $user = $currentLoggedUserGetter->getCurrentLoggedUser();

        // Acl system for checking rights
        $acl = new Acl($this->getServiceManager(), $auth, $user);
        if (!$acl->dispatch($e)) {
            $di = $this->getServiceManager()->get('di');
            /** @var FormatterProcessor $postProcessor */
            $formatter = $e->getRouteMatch()->getParam('formatter', false);
            $postProcessor = $di->get($formatter, array( //$formatter has to match with a key defined in module.config.php
                'response' => $e->getResponse(),
                'statusCode' => 403,
                'data' => array(
                    "error" => 403,
                    "message" => "Not enough rights"
                ),
            ));
        
            return $postProcessor->processAndGetResponse($e);
        }
        return null;
    }
}