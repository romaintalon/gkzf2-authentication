<?php

namespace GKZF2\Authentication;

use GKZF2\Authentication\Model\UserInterface;
use GKZF2\Authentication\Role\RoleManager;
use Zend\Feed\PubSubHubbub\HttpResponse;
use Zend\Mvc\Router\RouteMatch;
use Zend\Permissions\Acl\Acl as BaseAcl,
    Zend\Authentication\AuthenticationService,
    Zend\Mvc\MvcEvent,
    Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\RequestInterface;

class Acl extends BaseAcl {

    /**
     * Rôle par défaut
     */
    const DEFAULT_ROLE = 'guest';

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    protected $authenticationService;

    /**
     * @var UserInterface
     */
    protected $user;

    protected $_resources;
    protected $_default_role;

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @param \Zend\Authentication\AuthenticationService $authenticationService
     * @param UserInterface|null $user
     */
    public function __construct(ServiceManager $serviceManager, AuthenticationService $authenticationService, $user) {
        $this->serviceManager = $serviceManager;
        $this->authenticationService = $authenticationService;
        $this->user = $user;

        $this->initRoles();
    }

    /**
     * @param MvcEvent $e
     * @return bool
     * @throws \Exception
     */
    public function dispatch(MvcEvent $e) {
        $this->build();

        $queryRole = filter_input(INPUT_GET, "role");
        if ($queryRole !== null) {
            // check user has access to queryRole
            if (!$this->user->hasRole($queryRole)) {
                return false;
            } else {
                $role = $queryRole;
            }
        } else {
            if ($this->user) {
                $role = RoleManager::getHigherRole($this->user->getRoles());
            } else {
                $role = $this->_default_role;
            }
        }

        // Si l'utilisateur n'est pas autorisé, on le redirige vers la page par défaut, ou une autre page si elle a été spécifiée
        // dans le fichier de configuration

        $controller = $e->getRouteMatch()->getParam('controller');
        // action called ?
        $action  = $this->getActionName($e);

        if (!array_key_exists($controller, $this->_resources)) {
            throw new \Exception("Resource $controller $action does not exists");
        }
        if (!array_key_exists($action, $this->_resources[$controller])) {
            if (!array_key_exists('default', $this->_resources[$controller])) {
                throw new \Exception("Resource $controller $action does not exists");
            }
            $action = 'default';
        }

        $action_resources = $this->_resources[$controller][$action];

        if (!$this->isAllowed($role, "{$controller} {$action}")) {
            if (array_key_exists('redirectTo', $action_resources)) {
                $this->redirect($e, $action_resources['redirectTo']);
            } elseif (array_key_exists('error', $action_resources)) {
                return false;
            } elseif (array_key_exists('redirectTo', $this->_resources[$controller]['default'])) {
                $this->redirect($e, $this->_resources[$controller]['default']['redirectTo']);
            } elseif (array_key_exists('error', $this->_resources[$controller]['default'])) {
                return false;
            } else {
                throw new \Exception("No redirect neither error for this case");
            }
        }
        return true;
    }

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @return Acl
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;

        return $this;
    }

    /**
     * Initialise les rôles à partir du fichier de configuration
     */
    private function initRoles() {

        $configuration = $this->serviceManager->get('Configuration');

        if (isset($configuration['acl']['roles'])) {
            $roles = $configuration['acl']['roles'];

            foreach ($roles as $role => $parents) {
                $this->addRole($role, $parents);
            }
        }

        if (isset($configuration['acl']['default_role'])) {
            $this->_default_role = $configuration['acl']['default_role'];
        }
    }

    private function build() {
        // Récupération de la configuration
        $configuration = $this->serviceManager->get('Configuration');

        if (isset($configuration['acl']['resources'])) {
            $this->_resources = $configuration['acl']['resources'];
            $resources = $configuration['acl']['resources'];

            foreach ($resources as $controller => $actions) {
                $this->addResource("{$controller} default");
                foreach ($actions as $action => $rights) {
                    if ($action != 'default') {
                        $this->addResource("{$controller} {$action}", "{$controller} default");
                    }
                    if (array_key_exists('allow', $rights) && array_key_exists('roles', $rights['allow'])) {
                        foreach ($rights['allow']['roles'] as $role) {
                            $this->allow($role, "{$controller} {$action}");
                        }
                    }
                    if (array_key_exists('deny', $rights)) {
                        foreach ($rights['deny'] as $role) {
                            $this->deny($role, "{$controller} {$action}");
                        }
                    }
                }
            }
        } else {
            $this->_resources = array();
        }

    }

    /**
     * @param \Zend\Mvc\MvcEvent $e
     * @param string $route
     */
    private function redirect(MvcEvent $e, $route) {
        $url = $e->getRouter()->assemble(array(), array('name' => $route));
        /** @var HttpResponse $response */
        $response = $e->getResponse();
        $response->getHeaders()->addHeaderLine('Location', $url);
        $response->setStatusCode(302);
        $response->sendHeaders();

        exit;
    }

    /**
     * @param MvcEvent $e
     * @return string
     */
    private function getActionName(MvcEvent $e) {
        $action = $e->getRouteMatch()->getParam('action', false);
        if ($action) {
            return $action;
        }
        // RESTful methods
        $routeMatch = $e->getRouteMatch();
        /** @var \HttpRequest|RequestInterface $request */
        $request = $e->getRequest();
        $method = strtolower($request->getMethod());
        switch ($method) {
            // DELETE
            case 'delete':
                // GET
            case 'get':
                // PATCH
            case 'patch':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    return $method;
                }
                return "{$method}List";
            // POST
            case 'post':
                return 'create';
            // PUT
            case 'put':
                $id   = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    return 'update';
                }
                return 'replaceList';
            // HEAD
            // OPTIONS
            default:
                return $method;
            case 'head':
                return 'head';
            case 'options':
                return 'options';
        }
    }

    /**
     * @param RouteMatch $routeMatch
     * @param RequestInterface|\HttpRequest $request
     * @return bool|string
     */
    protected function getIdentifier(RouteMatch $routeMatch, RequestInterface $request)
    {
        $identifier = 'id';
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $id;
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $id;
        }

        return false;
    }
}
