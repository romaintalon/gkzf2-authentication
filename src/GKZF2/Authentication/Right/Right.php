<?php

namespace GKZF2\Authentication\Right;

abstract class Right {
    /**
     * @return bool
     */
    abstract function canBeAnonymous();

    /**
     * @param Right $right
     * @return bool
     */
    abstract function equals(Right $right);
}