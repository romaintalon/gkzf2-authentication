<?php

namespace GKZF2\Authentication\Right;

use GKZF2\Authentication\Model\UserInterface;

class RightManager {

    /**
     * @param UserInterface $user
     * @param Right $wantedRight
     * @return bool
     */
    public static function hasRight(UserInterface $user, Right $wantedRight) {
        if (!$user) {
            return $wantedRight->canBeAnonymous();
        }

        foreach ($user->getRoles() as $role) {
            foreach ($role->getRights() as $right) {
                if ($right->equals($wantedRight)) {
                    return true;
                }
            }
        }
        return false;
    }
}