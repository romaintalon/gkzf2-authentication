<?php

namespace GKZF2\Authentication\Model;

use GKZF2\Authentication\Role\RoleAbstract;

interface UserInterface {

    /**
     * @return RoleAbstract[]
     */
    public function getRoles();

    /**
     * @param RoleAbstract $roleInterface
     * @return bool
     */
    public function hasRole(RoleAbstract $roleInterface);
}