<?php

namespace GKZF2\Authentication\Model;

use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;

interface CurrentLoggedUserGetterInterface {
    /**
     * @return UserInterface
     */
    public function getCurrentLoggedUser();
}