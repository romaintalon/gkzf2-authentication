<?php

namespace GKZF2\Authentication;

use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceManager;

class Auth {

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    protected $authenticationService;

    public function getAuthenticationService() {
        return $this->authenticationService;
    }

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @throws \Exception
     */
    public function dispatch() {
        $configuration = $this->serviceManager->get('Configuration');

        $auth = new AuthenticationService();
        $this->authenticationService = $auth;
    }
}
